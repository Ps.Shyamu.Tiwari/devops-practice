FROM store/oracle/serverjre:1.8.0_241-b07
RUN groupadd -r assignment && useradd -r assignment -g assignment
ADD ./target/*.jar ./run.sh /home/
RUN chown -R assignment:assignment /home
RUN chmod -R +x /home/
WORKDIR /home 
USER assignment
EXPOSE 8090
ENTRYPOINT ["./run.sh"]